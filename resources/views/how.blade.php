<!doctype html>
<html lang="en">
    <head>
        @include('layout.header')
        <title>Social Sports TT: How it Works?</title>
    </head>
    <body>
        <div class="container">
            @include('layout.menu')

            <div class="jumbotron">
                <h1>How it works?</h1>
                <p>Well, right now it doesn't!</p>
            </div>

            @include('layout.footer')
        </div>
    </body>
</html>
