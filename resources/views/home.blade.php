<!doctype html>
<html lang="en">
    <head>
        @include('layout.header')
        <title>Social Sports: Table Tennis</title>
    </head>
    <body>
        <div class="container">
        @include('layout.menu')

            <div class="jumbotron">
                <h1>About Social Sports <small>TT</small></h1>
                <p>Social Sports lets you reimagine your favorite sports in the digital age.<br />It empowers you by carefully recording and presenting your sports activity data in a meaningful way, which you can then use to not only become a better player, but also to compete with fellow players.</p>
                <p>Interested?</p>
                <a class="btn btn-lg btn-success" href="/login">Log In</a>&nbsp;<a class="btn btn-lg btn-primary" href="/signup">Sign Up</a>
            </div>

            @include('layout.footer')
        </div>
    </body>
</html>
