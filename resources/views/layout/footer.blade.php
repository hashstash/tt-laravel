            <footer class="footer">
                <div class="container">
                    <small>
                    <p class="pull-left">&copy; <?php echo date('Y'); ?>, <a href="http://hashstash.in" target="_blank">Hashstash</a></p>
                    <p class="pull-right text-right">Written with <a href="http://www.laravel.com" target="_blank">Laravel 5</a> and <a href="http://sublimetext.com" target="_blank">Sublime Text 3</a><br />Standing on the shoulders of the Giants</p>
                    </small>
                </div>
            </footer>
