            <nav class="navbar navbar-default" role="navigation">
                <ul class="nav navbar-nav">
                    <li><a class="text-center text-uppercase" href="/"><strong>Social Sports <small>TT</small></strong></a></li>
                </ul>

                <!-- Static Page URLS -->
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="/how">How it Works?</a></li>
                    <li><a href="/ranking">Global Rankings</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login">Log in</a></li>
                    <li><a href="/signup">Sign Up</a></li>
                    <li><a href="/logout">Log Out</a></li>
                </ul>
            </nav>
