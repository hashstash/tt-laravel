<!DOCTYPE html>
<html>
    <head>

        @include('layout.header')
        <title>Your Quest Does Not Compute</title>
    </head>
    <body>
        <div class="container">
        @include('layout.menu')
            <div class="content">
                <div class="title">Your Quest Does Not Compute</div>
            </div>
        </div>
        @include('layout.footer')
    </body>
</html>
