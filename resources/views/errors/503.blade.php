<!DOCTYPE html>
<html>
    <head>
        @include('layout.header')

        <title>Be right back.</title>
    </head>
    <body>
        <div class="container">
        @include('layout.menu')
            <div class="content">
                <div class="title">Be right back.</div>
            </div>
        </div>
        @include('layout.footer')
    </body>
</html>
