<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

/***
 *  All static home level URLs
 */
Route::get('/home', function () {
    return view('home');
});

Route::get('/how', function () {
    return view('how');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/logout', function () {
    return view('logout');
});

Route::get('/signup', function () {
    return view('signup');
});

Route::get('/ranking', function () {
    return view('ranking');
});

/***
 *  All player (user) URLs
 */
Route::get('/player', function () {
    // return the player dashboard
    // replace {player} with player id in future
    // eg: http://tt.socialsports.com/kinshuksunil
    return view('player/dash');
});

Route::get('/player/edit', function () {
    // allow to edit player profile
    // replace {player} with player id in future
    // eg: http://tt.socialsports.com/kinshuksunil/edit
    return view('player/edit');
});

Route::get('/player/history', function () {
    // show player's match statistics
    // replace {player} with player id in future
    // eg: http://tt.socialsports.com/kinshuksunil/history
    return view('player/history');
});

Route::get('/player/vs/opponent', function () {
    // show player's record against a specific opponent
    // replace {player} with player id in future
    // // replace {opponent} with player id in future
    // eg: http://tt.socialsports.com/kinshuksunil/vs/madcaploon
    return view('player/opponent');
});

/***
 *  All admin URLs
 */
Route::get('/admin', function () {
    return view('admin/dash');
});

Route::get('/admin/players', function () {
    // returns list of all players in the system
    return view('admin/players');
});

Route::get('/admin/teams', function () {
    // returns list of all teams in the system
    return view('admin/teams');
});

Route::get('/admin/leagues', function () {
    // returns list of all leagues in the system
    return view('admin/leagues');
});

Route::get('/admin/matches', function () {
    // returns list of all matches in the system
    return view('admin/matches');
});

Route::get('/admin/system', function () {
    // system variables configuration
    return view('admin/system');
});

/***
 *  All match URLs
 */
Route::get('/match/id', function () {
    // return details of a match with {id}
    return view('match/details');
});

Route::get('/match/add', function () {
    // add a new match record in the system
    return view('match/add');
});

/***
 *  Route Group definitions
 */
Route::group(['middleware' => ['web']], function () {
    //
});
