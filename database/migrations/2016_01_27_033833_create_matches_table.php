<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('matches', function(Blueprint $table){
            $table->increments('id')->unique();
            $table->timestamp('happened_on');
            $table->integer('playerA_id');
            $table->integer('playerA_score');
            $table->integer('playerB_id');
            $table->integer('playerB_score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('matches');
    }
}
